# Ball on pillars

# Description

A ball is supposed to move over a shaft of columns.

## Rules

1. A ball should move horizontally.
1. The ball should be at the bottom of a wave
1. The shaft should consist of vertical bars
1. The bars rise and fall

## Interaction

- None

## Used Methods

- Sin/Cos-Function
- Recursion
- Relative coordinate system (Push-/PopMatrix())
- Recording Mode (Press "r")

![](animation.gif)
