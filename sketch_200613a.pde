int balkenWidth;
int balkenMaxHeight;
int balkenMinHeight;
int ballRadius;
ArrayList<Balken> balkenList;

void setup(){
  size(600,600);
  balkenWidth = 5;
  ballRadius = 40;
  balkenMaxHeight = height/2;
  balkenMinHeight = balkenMaxHeight-height/10;
  balkenList = new ArrayList<Balken>();
  erstelleBalken();
}

void draw(){
  background(255);
  maleBalken();
  maleBall();
}

void erstelleBalken(){
  // Anzahl Balken ermitteln
  int anzahlBalken = floor(width / (balkenWidth*1.5));
  
  // Alle Balken erstellen
  int x,y,w,h;
  for(int i=0;i<anzahlBalken;i++){
    //h = floor((height/2)*abs(sin(frameCount)));
    h = floor((height/2));
    x = floor(i*balkenWidth*1.5);
    y = height/2;
    balkenList.add(new Balken(x,y,balkenWidth,h));
  }
  
}
void maleBalken(){
  // Alle Balken malen
  for(Balken balken: balkenList){
    balken.draw();
  }
}

void maleBall(){
  Ball ball = new Ball(width/2,height/2,40,color(255,0,0));
  ball.draw();
}

class Ball{
  int r;
  int x, y;
  color c;
  
  Ball(int posX, int posY, int radius, color c){
    this.r = radius;
    this.x = posX;
    this.y = posY;
    this.c = c;
  }
  
  void draw(){
    ausrichten();
    noStroke();
    fill(c);
    circle(x, y, r);
  }
  
  void ausrichten(){
    Balken niedrigsterBalken = balkenList.get(0);
    for(Balken b: balkenList){
      if(b.h<niedrigsterBalken.h){
        niedrigsterBalken = b;
      }
    }
    this.x = niedrigsterBalken.x;
    this.y = height-niedrigsterBalken.h-ballRadius/2;
  }
}

class Balken{
  private int pos;
  int x,y;
  int w,h;
  color c = color(0);
  
  Balken(int x, int y, int w, int h){
    this.x=x;
    this.y=y;
    this.w=w;
    this.h=h;
    this.pos=balkenList.size()+1;
  }
  
  void draw(){
    noStroke();
    fill(c);
    calcNewHeight();
    println(h);
    rect(x, height-h, w, h);
  }
  
  void calcNewHeight(){
    float diff = balkenMaxHeight-balkenMinHeight;
    this.h = floor(abs(balkenMinHeight+diff*sin(this.pos*0.2+frameCount*0.02)));
  }
  
}
